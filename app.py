#  Copyright 2023 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Start the application for SensAI Expanse Bot 🤖 on the Telegram platform!

Application starter and polling for the Web service interface.
Load the token given by the BotFather, register the handlers,
and start polling.
"""

import html
import json
import logging
import re
import traceback
from os import environ

from dotenv import load_dotenv

# PyCharm annoying bug regarding package vs module names mismatch!
# noinspection PyPackageRequirements
from telegram import Update

# noinspection PyPackageRequirements
from telegram.constants import ParseMode

# noinspection PyPackageRequirements
from telegram.ext import (
    Application,
    ApplicationHandlerStop,
    ChatMemberHandler,
    CommandHandler,
    ContextTypes,
    MessageHandler,
    filters,
)

from basics import chat_member, chat_sensai

# ENVIRONMENT variables from .env without overriding previously set.
load_dotenv(override=False)

try:
    DEBUG = bool(environ["DEBUG"])
except KeyError:
    DEBUG = False

# Enable logging.
logging.basicConfig(level=logging.DEBUG if DEBUG else logging.INFO)
logger = logging.getLogger(__name__)
if logger.getEffectiveLevel() == logging.DEBUG:
    # noinspection SpellCheckingInspection
    logger_format = "%(asctime)s | %(name)s | %(funcName)s | %(levelname)s | %(message)s"
else:
    # noinspection SpellCheckingInspection
    logger_format = "%(asctime)s | %(name)s | %(levelname)s | %(message)s"
logging.basicConfig(format=logger_format, force=True)

HANDLER_PRIORITY_GROUP = -1

# Telegram ID of the agent able to issue special administrative commands.
try:
    SENSEI_ID_TG = environ["SENSEI_ID_TG"]
except KeyError:
    logger.exception(
        "A SENSEI_ID_TG entry in .env with a Telegram ID of the sensei (you) is required."
        " Sensei is an agent able to issue special administrative commands (e.g., /status)."
        " Alternatively, set environment variable SENSEI_ID_TG=nine-digit-id before run.",
    )
    raise

# Bot ID token issued by the BotFather.
try:
    BOT_ID_TOKEN = environ["BOT_ID_TOKEN"]
except KeyError:
    logger.exception(
        "A BOT_ID_TOKEN entry in .env with an auth token issued by the BotFather is required."
        " See https://core.telegram.org/bots#how-do-i-create-a-bot."
        " Alternatively, set environment variable BOT_ID_TOKEN=bot-issued-id before run.",
    )
    raise


async def sensei_check(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """
    Deal with privileged commands for SensAI's sensei eyes only.

    All commands except ``/help`` and ``/start`` are privileged to the SensAI's
    sensei.

    :param update: Chat update of any type such as including a text message.
    :param context: The update's context (e.g., chat environment).
    """
    chat = update.effective_chat
    user = update.effective_user
    message = update.effective_message
    if chat is None or user is None or message is None:
        return
    logger.debug(
        "Chat type [%s] | Chat ID: [%s] | User ID: [%d, %s] | Bot: [%s]",
        chat.type,
        chat.id,
        user.id,
        user.full_name,
        context.bot.username,
    )
    logger.debug("\n* * * * *\n%s\n* * * * *", update.effective_message)
    # Stop processing handlers (e.g., commands) if not sensei issuing privileged commands.
    if (
        user.id != int(SENSEI_ID_TG)
        and re.match("(/help,/start)( |@|$)", message.text if message.text is not None else "")
        is None
    ):
        await message.reply_text(
            quote=True,
            text="🤖 You are NOT my sensei!",
        )
        raise ApplicationHandlerStop


async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """
    Help by messaging a description and set bot commands.

    :param update: Chat update of any type such as including a text message.
    :param context: The update's context (e.g., chat environment).
    """
    chat = update.effective_chat
    user = update.effective_user
    if chat is None or user is None:
        return
    logger.debug(
        "Chat type [%s] | Chat ID: [%s] | User ID: [%d, %s] | Bot: [%s]",
        chat.type,
        chat.id,
        user.id,
        user.full_name,
        context.bot.username,
    )
    await context.bot.set_my_commands(
        commands=[
            ("help", "Show the bot description."),
            ("status", "Show which chats the bot is currently engaged in."),
            ("echo", "Echo the text message."),
            ("process", "Process (emoji to emoticon, translation) the text message."),
            ("sentiment", "Get sentiment from the text message."),
        ],
        language_code="en",
    )
    await context.bot.set_chat_menu_button()
    await context.bot.send_message(
        chat_id=chat.id,
        text=f"🤖 {(await context.bot.get_my_description()).description}",
    )


async def unknown_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """
    Inform of an unknown command.

    :param update: Chat update of any type such as including a text message.
    :param context: The update's context (e.g., chat environment).
    """
    chat = update.effective_chat
    user = update.effective_user
    if chat is None or user is None:
        return
    logger.debug(
        "Chat type [%s] | Chat ID: [%s] | User ID: [%d, %s] | Bot: [%s]",
        chat.type,
        chat.id,
        user.id,
        user.full_name,
        context.bot.username,
    )
    await context.bot.send_message(
        chat_id=chat.id,
        text="🤖 Command UNKNOWN!",
    )


async def error_handler(update: object, context: ContextTypes.DEFAULT_TYPE) -> None:
    """
    Log the error.

    If in DEBUG mode then send a Telegram message to notify the SensAI's sensei.

    :param update: Chat update of any type such as including a text message.
    :param context: The update's context (e.g., chat environment).
    """
    logger.exception("Exception while handling an update:", exc_info=context.error)
    if logger.getEffectiveLevel() == logging.DEBUG:
        # traceback.format_exception returns the usual python message about an exception, but as a
        # list of strings rather than a single string, so we have to join them together.
        tb_list = traceback.format_exception(
            None,
            context.error,
            None,
        )
        tb_string = "".join(tb_list)
        # Build the message with some markup and additional information about what happened.
        update_str = update.to_dict() if isinstance(update, Update) else str(update)
        update_str = json.dumps(update_str, indent=2, ensure_ascii=False)
        message = (
            f"An exception was raised while handling an update!\n"
            f"<pre>update = {html.escape(update_str)}</pre>\n\n"
            f"<pre>context.chat_data = {html.escape(str(context.chat_data))}</pre>\n\n"
            f"<pre>context.user_data = {html.escape(str(context.user_data))}</pre>\n\n"
            f"<pre>{html.escape(tb_string)}</pre>"
        )
        # Finally, send the message if in DEBUG mode!
        await context.bot.send_message(
            chat_id=SENSEI_ID_TG,
            text=message,
            parse_mode=ParseMode.HTML,
        )


def main() -> None:
    """
    Start the bot application and register all the required handlers.

    Build the application with the token given by the BotFather.
    Add action handlers.
    Start polling. (CTRL+C to stop and exit)
    """
    application = Application.builder().token(BOT_ID_TOKEN).build()
    # Sensei security check: if not sensei user issuing the command then stop before next handler!
    application.add_handler(
        MessageHandler(filters.COMMAND, sensei_check),
        HANDLER_PRIORITY_GROUP,
    )
    # *** chat_member ***
    # Check and record the user as being in a private chat with bot.
    application.add_handler(CommandHandler("start", chat_member.start_private_chat))
    # If no `/start` command then the first message will register the user.
    application.add_handler(
        MessageHandler(filters.ALL & ~filters.COMMAND, chat_member.start_private_chat),
        HANDLER_PRIORITY_GROUP,
    )
    application.add_handler(CommandHandler("status", chat_member.status))
    application.add_handler(
        ChatMemberHandler(chat_member.track_chats, ChatMemberHandler.MY_CHAT_MEMBER),
    )
    application.add_handler(
        ChatMemberHandler(
            chat_member.greet_chat_members,
            ChatMemberHandler.CHAT_MEMBER,
        ),
    )
    # *** chat_sensai ***
    application.add_handler(CommandHandler("echo", chat_sensai.echo))
    application.add_handler(CommandHandler("process", chat_sensai.process))
    application.add_handler(CommandHandler("sentiment", chat_sensai.sentiment))
    application.add_handler(
        MessageHandler(filters.TEXT & ~filters.COMMAND, chat_sensai.expanse),
    )
    # Help, unknown commands, and error handlers.
    application.add_handler(CommandHandler("help", help_command))
    application.add_handler(MessageHandler(filters.COMMAND, unknown_command))
    application.add_error_handler(error_handler)
    # 'allowed_updates' handle *all* updates including `chat_member` updates.
    # To reset this, simply pass `allowed_updates=[]`.
    application.run_polling(allowed_updates=Update.ALL_TYPES)
    # TODO Webhooks vs. polling!


if __name__ == "__main__":
    main()
