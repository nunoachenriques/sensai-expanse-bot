#!/bin/bash

# PROTECT, SHOW, FAIL-FAST RETURN
set -euxo pipefail

# DEPENDENCIES
DOCKER=$(whereis -b docker | cut -d ' ' -f 2)
PIPENV=$(whereis -b pipenv | cut -d ' ' -f 2)
# COLOURS
ESC_SEQ="\x1b["
C_RESET="${ESC_SEQ}39;49;00m"
C_RED="${ESC_SEQ}31;01m"
C_GREEN="${ESC_SEQ}32;01m"

# CHECK DEPENDENCIES
if ! [ -x "${DOCKER}" ]; then
    echo -e "${C_RED}${DOCKER} ERROR! Hint: https://docs.docker.com/engine/install/${C_RESET}"
    exit 1
fi
if ! [ -x "${PIPENV}" ]; then
    echo -e "${C_RED}${PIPENV} ERROR! Hint: pipx install pipenv${C_RESET}"
    exit 1
fi

# QUALITY ASSURANCE requirement
${PIPENV} run pre-commit run --all-files --hook-stage commit; status=$?
if [ ${status} -ne 0 ]; then
    echo -e "${C_RED}Quality assurance (commit stage) ERROR! Exit status: ${status}!${C_RESET}"
    exit 1
fi
${PIPENV} run pre-commit run --all-files --hook-stage push; status=$?
if [ ${status} -ne 0 ]; then
    echo -e "${C_RED}Quality assurance (push stage) ERROR! Exit status: ${status}!${C_RESET}"
    exit 1
fi

# BUILD
# CONTAINER_REGISTRY* are provided by ENV if required (e.g., push to the registry)!
#     else an empty string is assigned (default: "") for a simple local docker tag.
CONTAINER_REGISTRY=${CONTAINER_REGISTRY:-""}
CONTAINER_REGISTRY_GROUP=${CONTAINER_REGISTRY_GROUP:-""}
VERSION=$(tr -d "[:space:]" < VERSION)
NAME=$(basename -s .git "$(git config --get remote.origin.url)")
TAG="${CONTAINER_REGISTRY}${CONTAINER_REGISTRY_GROUP}${NAME}:${VERSION}"
# Permission denied ERROR? See https://docs.docker.com/engine/install/linux-postinstall/
${DOCKER} build --label version="${VERSION}" -t "${TAG}" .; status=$?
if [ ${status} -ne 0 ]; then
    echo -e "${C_RED}${DOCKER} build ${TAG} ERROR! Exit status: ${status}!${C_RESET}"
    exit 1
fi

echo -e "Docker build ${TAG}: ${C_GREEN}100%${C_RESET}"
exit 0
