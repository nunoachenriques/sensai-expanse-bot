[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")](http://www.apache.org/licenses/LICENSE-2.0.html)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![code style - black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/charliermarsh/ruff/main/assets/badge/v1.json)](https://github.com/charliermarsh/ruff)
[![types - Mypy](https://img.shields.io/badge/types-Mypy-blue.svg)](https://github.com/python/mypy)

# SensAI Expanse Bot 🤖 for Telegram

A [Telegram](https://telegram.org/) [bot](https://core.telegram.org/bots) for
sentiment analysis, i.e., a sentiment analysis bot making use of the
[Telegram Bot API](https://core.telegram.org/bots/api). Just add the
[SensAI Expanse bot](https://t.me/sensai_expanse_bot) to a Telegram group and
start writing messages. The bot will analyse the messages and use all that data
to access your emotional valence, i.e., if you feel positive, neutral,
or negative.

**CAVEAT:** Sentiment analysis supported languages are configure in the
`basics/chat_sensai.py` file and restricted to (EN, FR, IT, PT, ES).
The [Lingua](https://github.com/pemistahl/lingua-py) language detector is
used. The 🤗 [Transformers](https://github.com/huggingface/transformers)
is for translation.
The [NLTK Vader](https://github.com/nltk/nltk/wiki/Sentiment-Analysis) is for
text sentiment analysis. Both Lingua and Transformers may be configured
to use dozens of languages and translate each one to canonical English.
However, at the expense of resources and processing time.

**WARNING:** Work in progress for experimental purposes...

**NOTICE:** Using UNIX shell commands in a Debian GNU/Linux Bash shell.
Adapt accordingly your Operating System.

## Content

* [Use](#use)
* [Develop](#develop)
  * [Fork](#fork)
  * [Prerequisites](#prerequisites)
  * [Quality Assurance](#quality-assurance)
  * [Wrap-up](#wrap-up)
  * [Run](#run)
* [History](#history)
* [License](#license)

## Use

**_TODO_**

## Develop

https://gitlab.com/nunoachenriques/sensai-expanse-bot

### Fork

In order to facilitate pull requests if you want to contribute then go to
https://gitlab.com/nunoachenriques/sensai-expanse-bot and hit **fork**!


### Prerequisites

**[Linux](docs/README-Linux.md)**

**[macOS](docs/README-macOS.md)**

**[Windows](docs/README-Windows.md)**

### Quality Assurance

**NOTICE:** Make sure you've completed the [Prerequisites](#prerequisites) for
your operating system case!

**SHORTCUT:** If you've completed the [Prerequisites](#prerequisites) AND
NOT interested in the details then YOU MAY SKIP this step-by-step guide and
JUMP to the next section [Wrap-up](#wrap-up).

**[Quality Assurance Step by Step](docs/README-QA-Steps.md)**

### Wrap-up

All the [prerequisites](#prerequisites) must be accomplished (by following
the above instructions or by means of a previous project installation).
The project files must be in place.

**First**, a **[fork](#fork)** of the remote. **Second**, the **clone** in
your own repository:

```shell
git clone git@gitlab.com:{YOUR_OWN_USER}/sensai-expanse-bot.git
```

```shell
cd sensai-expanse-bot
pipenv install --dev
pipenv run pre-commit install -t pre-commit
pipenv run pre-commit install -t pre-push
```

How to test the `.pre-commit-config.yaml` calls and also the configuration in
`pyproject.toml`:

```shell
pipenv run pre-commit run --all-files --hook-stage commit
pipenv run pre-commit run --all-files --hook-stage push
```

### Run

**NOTICE:** Using a `.env` file in the project root for the environment configuration:

```dotenv
# "" (empty string) = False, "ANY-VALUE" = True.
DEBUG=""

# Bot ID token issued by the BotFather.
BOT_ID_TOKEN="0123456789:XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

# Telegram ID of the agent (you?) able to test chat.
CHAT_ID_TESTS="123456789"

# Telegram ID of the agent able to issue special administrative commands.
SENSEI_ID_TG="123456789"
```

A **command-line interface** is available and ready to use for running the server:

```shell
pipenv run python app.py
```
Hit CTRL+C to stop and exit!

A **Docker** may be built and run. The 330MiB of tmpfs memory are for the
Transformers model ~301MiB, the NLTK Vader lexicon ~1MiB, and ~10% safe slack:

```shell
(. docker_build.sh)
set -a && source .env && set +a
docker run -it --rm --mount type=tmpfs,destination=/tmp,tmpfs-size=346030080 \
           -e TRANSFORMERS_CACHE=/tmp \
           -e NLTK_DATA=/tmp \
           -e BOT_ID_TOKEN="${BOT_ID_TOKEN}" \
           -e SENSEI_ID_TG="${SENSEI_ID_TG}" \
           --name=sensai-expanse-bot \
           sensai-expanse-bot:$(tr -d "[:space:]" < VERSION)
```

## History

An experimental exercise inspired by the
[SensAI](https://gitlab.com/nunoachenriques/sensei) and
[SensAI Expanse](https://gitlab.com/nunoachenriques/sensei-expanse).
It took place during a career break in 2023.

## License

Copyright 2023 Nuno A. C. Henriques https://nunoachenriques.net

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
