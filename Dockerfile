FROM python:3.10-slim AS base
ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONFAULTHANDLER=1
ENV PYTHONUNBUFFERED=1
ENV APP_PATH=/app
ENV APP_USER=appuser

LABEL name="SensAI Expanse Bot 🤖 for Telegram" \
      description="A sentiment analysis bot making use of the Telegram platform" \
      maintainer="nunoachenriques.net"

FROM base AS python-app-deps

# INSTALL Python venv manager pipenv.
RUN pip install pipenv
# NON-root USER.
RUN useradd ${APP_USER} && mkdir ${APP_PATH} && chown -R ${APP_USER} ${APP_PATH}
USER ${APP_USER}
# INSTALL Python app dependencies.
COPY --chown=${APP_USER}:${APP_USER} Pipfile ${APP_PATH}
COPY --chown=${APP_USER}:${APP_USER} Pipfile.lock ${APP_PATH}
RUN cd ${APP_PATH} && PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy

FROM base AS python-app-run

WORKDIR ${APP_PATH}
# NON-root USER
RUN useradd ${APP_USER} && chown -R ${APP_USER} ${APP_PATH}
# COPY virtual ENV from python-app-deps stage
COPY --chown=${APP_USER}:${APP_USER} --from=python-app-deps ${APP_PATH}/.venv ${APP_PATH}/.venv
ENV PATH="${APP_PATH}/.venv/bin:${PATH}"
# INSTALL app into container
COPY --chown=${APP_USER}:${APP_USER} . ${APP_PATH}
# NON-root USER
USER ${APP_USER}

# RUN app
CMD ${APP_PATH}/.venv/bin/python app.py
