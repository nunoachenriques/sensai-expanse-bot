#  Copyright 2023 Nuno A. C. Henriques https://nunoachenriques.net
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Test the py:mod:`chat_sensai` module.

This is an integration test.
"""

import logging
from os import environ

import pytest
from dotenv import load_dotenv

# ENVIRONMENT variables from .env without overriding previously set.
load_dotenv(override=False)

# Enable logging
logger = logging.getLogger(__name__)

try:
    CHAT_ID_TESTS = environ["CHAT_ID_TESTS"]
except KeyError:
    logger.exception(
        "A CHAT_ID_TESTS entry in .env with a Telegram ID of the sensei (you) is required."
        " Sensei is an agent able to test chat."
        " Alternatively, set environment variable CHAT_ID_TESTS=nine-digit-id before run.",
    )
    raise


@pytest.mark.asyncio()
class TestChatSensai:
    """TODO Test the py:mod:`chat_sensai` module."""

    @pytest.mark.skip()
    async def test_echo(self: "TestChatSensai") -> None:
        """TODO Test the py:func:`echo`."""


if __name__ == "__main__":
    pytest.main()
