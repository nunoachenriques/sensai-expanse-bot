#  Copyright 2023 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Feed the application for SensAI Expanse Bot 🤖 with message handlers.

It handles chat commands (e.g., /echo) and messages from the user.
The expanse means to process the messages in an affective sense
(e.g., sentiment analysis).
"""

import logging
from datetime import datetime, timedelta, timezone

import emoji
import emoji_data_python as edp
import nltk
import numpy as np
from emoji_data_python import emoji_data as edp_data

# PyCharm annoying bug regarding package vs module names mismatch!
# noinspection PyPackageRequirements
from lingua import Language, LanguageDetectorBuilder
from nltk.sentiment.vader import SentimentIntensityAnalyzer

# noinspection PyPackageRequirements
from telegram import ChatMember, Update

# noinspection PyPackageRequirements
from telegram.constants import ChatAction, ParseMode

# noinspection PyPackageRequirements
from telegram.ext import CallbackContext, ContextTypes
from transformers import pipeline

# Enable logging
logger = logging.getLogger(__name__)

# Build language detector and translation models for multi-language (restricted, adapt accordingly).
LINGUA_LANGUAGES = [
    Language.ENGLISH,
    Language.FRENCH,
    Language.ITALIAN,
    Language.PORTUGUESE,
    Language.SPANISH,
]
lingua = (
    LanguageDetectorBuilder.from_languages(*LINGUA_LANGUAGES)
    .with_preloaded_language_models()
    .build()
)
# Transformers translation from Romance (EN, FR, IT, PT, ES, ...) to English
translate = pipeline(task="translation", model="Helsinki-NLP/opus-mt-roa-en")
# VADER Lexicon for sentiment analysis
nltk.download("vader_lexicon")
vader = SentimentIntensityAnalyzer()
# Polarity bounds
NEGATIVE_HIGH = -1.0
NEGATIVE_LOW = -0.5
POSITIVE_LOW = 0.5
POSITIVE_HIGH = 1.0
# Polarity class
NEGATIVE = "😞"
NEUTRAL = "😐"
POSITIVE = "😊"
# From https://nunoachenriques.net/sensai+expanse/nachenriques-2020-phd-dissertation.pdf p.73-74
SENTIMENT_DURATION = timedelta(hours=1)
SENTIMENT_ANALYSIS_PERIOD = timedelta(minutes=15)
SENTIMENT_ANALYSIS_START_DELAY = timedelta(minutes=1)


def _emoji_to_emoticon(e: str, _: dict[str, str]) -> str:
    """
    Convert Unicode emoji (e.g., "😉") to text emoticon (e.g., ":-)").

    A callback function for ``emoji.replace_emoji(str, str | callback)``.

    The ``emoji`` package data doesn't include emoticon ``text`` alias thus
    ``emoji_data`` from ``emoji_python_data`` package has to be used.
    Moreover, search first for the ``text`` single alias. If null then search
    in ``texts`` field which may contain several alias. Both may be null.

    :param e: Emoji 😉 to convert to emoticon :-).
    :param _: The ``emoji.EMOJI_DATA`` record of ``e``.
    :return: Emoji text alias (e.g., ":-)") or empty string if none.
    """
    logger.debug("\n* * * * *\n%s\n* * * * *", e)
    # Search for emoji's emoticon alias in `text` field of the emoji_data_python data.
    emoticon = [
        ed.text for ed in edp_data if ed.text is not None and edp.char_to_unified(e) == ed.unified
    ]
    # Search for emoji's emoticon alias in `texts` field of the emoji_data_python data.
    if emoticon is None:
        emoticon = next(
            ed.texts
            for ed in edp_data
            if ed.texts is not None and edp.char_to_unified(e) == ed.unified
        )
    # Return the last found if not none.
    if emoticon is not None and len(emoticon) > 0:
        return emoticon[-1]
    return ""


def _polarity_to_emoji(polarity: float) -> str | None:
    """
    Convert the polarity value to the corresponding emoji.

    The polarity value from VADER is comprised in [-1, 1]::

     [-1.0, -0.5] = Negative class = -1 = 😞
     (-0.5,  0.5) = Neutral class  =  0 = 😐
     [ 0.5,  1.0] = Positive class =  1 = 😊

    :param polarity: Emotional valence value.
    :return: Polarity converted to one of the available classes as an emoji.
        None if the value is None or out of bounds.
    """
    if polarity is None:
        return None
    if NEGATIVE_HIGH <= polarity <= NEGATIVE_LOW:
        return NEGATIVE
    if NEGATIVE_LOW < polarity < POSITIVE_LOW:
        return NEUTRAL
    if POSITIVE_LOW <= polarity <= POSITIVE_HIGH:
        return POSITIVE
    return None


def _message_to_analysis(message: str) -> str:
    """
    Process the user text message for sentiment analysis.

    The process is adapted from https://arxiv.org/abs/1912.10084 Fig. 4::

                             /text message/
                                   |
                        [convert emoji to emoticon]
                                   |
                         [auto-detect languages]
                                   |
               [translate to sentiment-supported language]

    :param message: Chat update text message from user.
    :return: The user text message in a suitable format for sentiment analysis.
    """
    # Convert emoji to emoticon.
    text = emoji.replace_emoji(message, _emoji_to_emoticon)
    # Detect languages.
    text_languages = lingua.detect_multiple_languages_of(text)
    # Translate each language chunk to English. Join all translations into one text message.
    text_translation = []
    for tl in text_languages:
        text_translation.append(
            # https://black.readthedocs.io/en/stable/the_black_code_style/current_style.html#slices
            translate(text[tl.start_index : tl.end_index])[0]["translation_text"],
        )
    return " ".join(text_translation)


async def echo(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """
    Echo the user text message when the command ``/echo message`` is issued.

    Adapted from https://docs.python-telegram-bot.org/en/stable/examples.echobot.html

    :param update: Chat update of any type such as including a text message.
    :param context: The update's context (e.g., chat environment).
    """
    logger.debug("\n* * * * *\n%s\n* * * * *", update)
    if update.effective_chat is None or context.args is None:
        return
    text = " ".join(context.args)
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=text if len(text) > 0 else "<em>empty message</em>",
        parse_mode=ParseMode.HTML,
    )


async def process(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """
    Process the user text message when the command ``/process message`` is issued.

    * First, process (py:func:`_message_to_analysis`) the message.
    * Second, Send an update to the chat with the result.

    :param update: Chat update of any type such as including a text message.
    :param context: The update's context (e.g., chat environment).
    """
    logger.debug("\n* * * * *\n%s\n* * * * *", update)
    if update.effective_chat is None or context.args is None:
        return
    text = _message_to_analysis(" ".join(context.args))
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=text if len(text) > 0 else "<em>empty message</em>",
        parse_mode=ParseMode.HTML,
    )


async def sentiment(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """
    Get sentiment from the user text message by the command ``/sentiment message``.

    * First, process (py:func:`_message_to_analysis`) the message.
    * Second, get the polarity score.
    * Third, Send an update to the chat with the result.

    :param update: Chat update of any type such as including a text message.
    :param context: The update's context (e.g., chat environment).
    """
    logger.debug("\n* * * * *\n%s\n* * * * *", update)
    if update.effective_chat is None or context.args is None:
        return
    # Process the text message.
    text_translation = _message_to_analysis(" ".join(context.args))
    # Get a compound sentiment (polarity score).
    text_sentiment = vader.polarity_scores(text_translation)["compound"]
    # Protect from empty message.
    text = text_translation if len(text_translation) > 0 else "<em>empty message</em>"
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=f"🤖 {text}\n{text_sentiment:1.2f} {_polarity_to_emoji(text_sentiment)}",
        parse_mode=ParseMode.HTML,
    )


async def expanse(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """
    Process the user text message for sentiment analysis.

    Furthermore, it stores the sentiment analysis result along with some
    useful data for posterior analysis (e.g., sentiment in the last hour)::

     chat_data = {
         "expanse": {
             user_id: {
                 datetime: {
                     "polarity_score": float,
                     "sentiment": emoticon
                 }
                 ...
             }
             ...
         }
     }

    The ``chat_data`` is only available per chat (``chat_id`` where the bot is in).

    :param update: Chat update of any type such as including a text message.
    :param context: The update's context (e.g., chat environment).
    """
    user = update.effective_user
    message = update.effective_message
    job_queue = context.job_queue
    logger.debug("\n* * * * *\n%s\n* * * * *", user)
    logger.debug("\n* * * * *\n%s\n* * * * *", message)
    logger.debug("\n* * * * *\n%s\n* * * * *", job_queue)
    logger.debug("\n* * * * *\n%s\n* * * * *", context.chat_data)
    if user is None or message is None or job_queue is None or context.chat_data is None:
        return
    # Process the text message.
    text_translation = _message_to_analysis(str(message.text))
    # Get a compound sentiment (polarity score).
    text_sentiment = vader.polarity_scores(text_translation)["compound"]
    # Store sentiment analysis and data for context user (from any chat).
    context.chat_data.setdefault("expanse", {}).setdefault(user.id, {}).update(
        {
            message.date: {
                "polarity_score": text_sentiment,
                "sentiment": _polarity_to_emoji(text_sentiment),
            },
        },
    )
    logger.debug("\n* * * * *\n%s\n* * * * *", context.chat_data["expanse"])
    # Start the timer for the expanse_predict job per chat, if not yet running.
    current_jobs = job_queue.get_jobs_by_name(str(message.chat_id))
    if current_jobs:
        return
    jq = job_queue.run_repeating(
        expanse_predict,
        interval=SENTIMENT_ANALYSIS_PERIOD,
        first=SENTIMENT_ANALYSIS_START_DELAY,
        name=str(message.chat_id),
        chat_id=message.chat_id,
    )
    if jq is None:
        logger.warning("JOB QUEUE DIDN'T STARTED! NO `expanse_predict` AVAILABLE!")


async def expanse_predict(context: CallbackContext) -> None:
    """
    Predict emotional valence polarity scores-based.

    A naive prediction based on the sentiment analysis of the messages
    within the last ``SENTIMENT_DURATION`` minutes.

    :param context: The callback context (e.g., chat environment).
    """
    job = context.job
    chat_data = context.chat_data
    logger.debug("\n* * * * *\n%s\n* * * * *", job)
    logger.debug("\n* * * * *\n%s\n* * * * *", chat_data)
    if job is None or chat_data is None or "expanse" not in chat_data:
        return
    chat_data_expanse = chat_data["expanse"]
    chat_id = job.chat_id
    # Signal the chat that the bot is typing...
    await context.bot.send_chat_action(
        chat_id=chat_id,
        action=ChatAction.TYPING,
    )
    # For each user, calculate sentiment mean.
    for user_id in chat_data_expanse:
        user_data_r = reversed(chat_data_expanse[user_id].items())
        now = datetime.now(timezone.utc)
        polarities = []
        # Collect polarities, if within a time interval of interest.
        for message_time, sentiment_data in user_data_r:
            if now - message_time > SENTIMENT_DURATION:
                break
            polarities.append(sentiment_data["polarity_score"])
        # Calculate sentiment mean and send a message, if any polarities collected.
        if len(polarities) > 0:
            polarity_mean = float(np.mean(polarities))
            member: ChatMember = await context.bot.get_chat_member(chat_id=chat_id, user_id=user_id)
            user = member.user.mention_html()
            await context.bot.send_message(
                chat_id=chat_id,
                text=f"🤖 {user}\n{polarity_mean:1.2f} {_polarity_to_emoji(polarity_mean)}",
                parse_mode=ParseMode.HTML,
            )
