#  Copyright 2023 Nuno A. C. Henriques [nunoachenriques.net]
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

"""
Feed the application for SensAI Expanse Bot 🤖 with chat member handlers.

It handles chat member updates, greets new users, and keeps track of which
chats the bot is in.

Adapted from https://docs.python-telegram-bot.org/en/stable/examples.chatmemberbot.html
"""

import logging

# PyCharm annoying bug regarding package vs. module names mismatch!
# noinspection PyPackageRequirements
from telegram import Chat, ChatMember, ChatMemberUpdated, Update

# noinspection PyPackageRequirements
from telegram.constants import ParseMode

# noinspection PyPackageRequirements
from telegram.ext import ContextTypes

# Enable logging.
logger = logging.getLogger(__name__)


def _extract_status_change(
    chat_member_update: ChatMemberUpdated | None,
    context: ContextTypes.DEFAULT_TYPE,
) -> tuple[bool, bool] | None:
    """
    Extract the difference between the chat member previous status and the new one.

    Takes a ``ChatMemberUpdated`` instance and extracts whether the `old_chat_member`
    was a member of the chat and whether the 'new_chat_member' is a member of the chat.

    :param chat_member_update: Chat member status changes.
    :param context: The update's context (e.g., chat environment).
    :returns The logical value of both `was_member`, `is_member` as a tuple.
    None, if the status didn't change.
    """
    if chat_member_update is None:
        return None
    status_change = chat_member_update.difference().get("status")
    if status_change is None:
        return None
    old_status, new_status = status_change
    old_is_member, new_is_member = chat_member_update.difference().get(
        "is_member",
        (None, None),
    )
    was_member = old_status in [
        ChatMember.MEMBER,
        ChatMember.OWNER,
        ChatMember.ADMINISTRATOR,
    ] or (old_status == ChatMember.RESTRICTED and old_is_member is True)
    is_member = new_status in [
        ChatMember.MEMBER,
        ChatMember.OWNER,
        ChatMember.ADMINISTRATOR,
    ] or (new_status == ChatMember.RESTRICTED and new_is_member is True)
    logger.debug(
        "Member [%s] status just changed from [%s] to [%s]. Chat data: [%s]",
        chat_member_update.old_chat_member.user.full_name,
        was_member,
        is_member,
        context.chat_data,
    )
    return was_member, is_member


async def track_chats(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """
    Track the chats the bot is in.

    :param update: Chat update of any type such as including a text message.
    :param context: The update's context (e.g., chat environment).
    """
    if update.effective_chat is None or update.effective_user is None:
        return
    status_change = _extract_status_change(update.my_chat_member, context)
    if status_change is None:
        return
    was_member, is_member = status_change
    # Let's check who is responsible for the change
    cause_name = update.effective_user.full_name
    # Handle chat types differently:
    chat = update.effective_chat
    if chat.type == Chat.PRIVATE:
        if not was_member and is_member:
            # This may not be really needed. In practice most clients will
            # automatically send a `/start` command after the user unblocks
            # the bot, and start_private_chat() will add the user to "user_ids".
            # We're including this here for the sake of the example.
            logger.info("%s unblocked the bot.", cause_name)
            context.bot_data.setdefault("user_ids", set()).add(chat.id)
        elif was_member and not is_member:
            logger.info("%s blocked the bot.", cause_name)
            context.bot_data.setdefault("user_ids", set()).discard(chat.id)
    elif chat.type in [Chat.GROUP, Chat.SUPERGROUP]:
        if not was_member and is_member:
            logger.info("%s added the bot to the group [%s].", cause_name, chat.title)
            context.bot_data.setdefault("group_ids", set()).add(chat.id)
        elif was_member and not is_member:
            logger.info(
                "%s removed the bot from the group [%s].",
                cause_name,
                chat.title,
            )
            context.bot_data.setdefault("group_ids", set()).discard(chat.id)
    elif not was_member and is_member:
        logger.info("%s added the bot to the channel [%s].", cause_name, chat.title)
        context.bot_data.setdefault("channel_ids", set()).add(chat.id)
    else:  # elif was_member and not is_member:
        logger.info("%s removed the bot from the channel [%s].", cause_name, chat.title)
        context.bot_data.setdefault("channel_ids", set()).discard(chat.id)


async def status(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """
    Show which chats the bot is in.

    :param update: Chat update of any type such as including a text message.
    :param context: The update's context (e.g., chat environment).
    """
    if update.effective_message is None:
        return
    user_ids = ", ".join(str(uid) for uid in context.bot_data.setdefault("user_ids", set()))
    group_ids = ", ".join(str(gid) for gid in context.bot_data.setdefault("group_ids", set()))
    channel_ids = ", ".join(str(cid) for cid in context.bot_data.setdefault("channel_ids", set()))
    text = [f"@{context.bot.username}"]
    if user_ids:
        text.append(f"is currently in a conversation with the user IDs [{user_ids}].")
    else:
        text.append("is not in any conversation right now.")
    if group_ids:
        text.append(f"Moreover, it is a member of the groups with IDs {group_ids}.")
    else:
        text.append("Moreover, not a member of any groups.")
    if channel_ids:
        text.append(
            f"Further, it is an administrator in the channels with IDs {channel_ids}.",
        )
    else:
        text.append("Further, not any role in any channels.")
    await update.effective_message.reply_text(" ".join(text))


async def greet_chat_members(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """
    Greet new users in chats and announces when someone leaves.

    :param update: Chat update of any type such as including a text message.
    :param context: The update's context (e.g., chat environment).
    """
    if update.effective_chat is None or update.chat_member is None:
        return
    status_change = _extract_status_change(update.chat_member, context)
    if status_change is None:
        return
    was_member, is_member = status_change
    cause_name = update.chat_member.from_user.mention_html()
    member_name = update.chat_member.new_chat_member.user.mention_html()
    if not was_member and is_member:
        await update.effective_chat.send_message(
            text=rf"{member_name} was added by {cause_name}. Welcome!",
            parse_mode=ParseMode.HTML,
        )
    elif was_member and not is_member:
        await update.effective_chat.send_message(
            text=rf"{member_name} is no longer with us. {cause_name} is to blame or praise...",
            parse_mode=ParseMode.HTML,
        )


async def start_private_chat(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """
    Record the chat has started if it's a private chat.

    Since no `my_chat_member` update is issued when a user starts a private
    chat with the bot for the first time, it has to be tracked explicitly here.

    :param update: Chat update of any type such as including a text message.
    :param context: The update's context (e.g., chat environment).
    """
    chat = update.effective_chat
    user = update.effective_user
    if chat is None or user is None:
        return
    logger.debug("Chat type [%s] | Chat ID: [%s]", chat.type, chat.id)
    if chat.type != Chat.PRIVATE or chat.id in context.bot_data.get("user_ids", set()):
        return
    logger.info("%s started a private chat with %s.", user.full_name, context.bot.name)
    context.bot_data.setdefault("user_ids", set()).add(chat.id)
    await context.bot.send_message(
        chat_id=chat.id,
        text=rf"🤖 Hey, {user.mention_html()}!",
        parse_mode=ParseMode.HTML,
    )
